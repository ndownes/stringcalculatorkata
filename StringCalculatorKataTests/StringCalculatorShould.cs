﻿namespace StringCalculatorKataTests
{
    using FluentAssertions;
    using NUnit.Framework;
    using StringCalculatorKata;

    [TestFixture]
    public class StringCalculatorShould
    {
        private StringCalculator _stringCalculator;

        [SetUp]
        public void Setup()
        {
            _stringCalculator = new StringCalculator();
        }

        [TestCase("", 0)]
        [TestCase(",", 0)]
        public void ReturnZero_WhenAddingArguments_GivenEmptyInputs(string input, int number)
        {
            var sum = _stringCalculator.Add(input);

            sum.Should().Be(number);
        }

        [TestCase("1", 1)]
        [TestCase("2", 2)]
        [TestCase("10", 10)]
        [TestCase(",10", 10)]
        [TestCase("11,", 11)]
        public void ReturnInputAsInteger_WhenAddingArguments_GivenSingleInputAsString(string input, int number)
        {
            var sum = _stringCalculator.Add(input);

            sum.Should().Be(number);
        }

        [TestCase("1,2", 3)]
        [TestCase("5,2", 7)]
        [TestCase("2,5", 7)]
        [TestCase("4,18", 22)]
        public void ReturnSumOfTwoInputNumbers_WhenAddingArguments_GivenTwoInputsAsString(string input, int answer)
        {
            var sum = _stringCalculator.Add(input);

            sum.Should().Be(answer);
        }

        [TestCase("1,2,4", 7)]
        [TestCase("5,2,3,10", 20)]
        [TestCase("2,5,1,3,8,11", 30)]
        public void ReturnSumOfInputNumbers_WhenAddingArguments_GivenInputsAsString(string input, int answer)
        {
            var sum = _stringCalculator.Add(input);

            sum.Should().Be(answer);
        }

        [TestCase("1\n2,3", 6)]
        [TestCase("12\n3", 15)]
        [TestCase("1\n2\n1", 4)]
        [TestCase("2\n5\n1\n3\n8\n11", 30)]
        public void ReturnSumOfInputNumbers_WhenAddingArguments_GivenInputWithNewLineDelimiter(string inputs, int answer)
        {
            var sum = _stringCalculator.Add(inputs);

            sum.Should().Be(answer);
        }
    }
}

