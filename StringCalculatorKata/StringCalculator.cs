﻿using System;
using System.Linq;
using System.Xml.Schema;

namespace StringCalculatorKata
{
    public class StringCalculator
    {
        public int Add(string inputs)
        {
            var digits = inputs.Split(',', '\n');

            return digits.Sum(digit => digit.ToIntOrZero());
        }
    }

    public static class StringCalcultorExtensions
    {
        public static int ToIntOrZero(this string number)
        {
            int num;
            Int32.TryParse(number, out num);
            return num;
        }
    }
}